const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Gig = require('../models/Gig');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

// Get all gigs
router.get('/', (req, res) =>
  Gig.findAll()
    .then((gigs) => res.render('gigs', { gigs }))
    .catch((err) => res.render('error', { error: err }))
);

// Display add gig form
router.get('/add', (req, res) => res.render('add'));

// Add a gig
router.post('/add', (req, res) => {
  let {
    title,
    img,
    technologies,
    budget,
    description,
    contact_email,
  } = req.body;
  let errors = [];

  // Validate fields
  if (!title) {
    errors.push({ text: 'Please add a title' });
  }
  if (!technologies) {
    errors.push({ text: 'Please add some technologies' });
  }
  if (!description) {
    errors.push({ text: 'Please add a description' });
  }
  if (!contact_email) {
    errors.push({ text: 'Please add a contact email' });
  }

  // Check for errors
  if (errors.length > 0) {
    res.render('add', {
      title,
      img,
      technologies,
      budget,
      description,
      contact_email,
      errors,
    });
  } else {
    if (!budget) {
      budget = 'Unknown';
    } else {
      budget = `$${budget}`;
    }

    if (!img) {
      img =
        'http://gravatar.com/avatar/da26559f5423c3b91ede876f53bf4f6d?d=identicon';
    } else {
      console.log(img); // => just a file name
      // To-do: Upload image to s3 bucket
      //        and update img = img location in s3 bucket
    }

    // Make lowercase and remove space after comma
    technologies = technologies.toLowerCase().replace(/, /g, ',');

    // Insert into table
    Gig.create({
      title,
      img,
      technologies,
      budget,
      description,
      contact_email,
    })
      .then((gig) => res.redirect('/gigs'))
      .catch((err) => res.render('error', { error: err }));
  }
});

// Search for gigs
router.get('/search', (req, res) => {
  let { term } = req.query;

  term = term.toLowerCase();

  Gig.findAll({
    where: {
      technologies: {
        [Op.like]: `%${term}%`,
      },
    },
  })
    .then((gigs) => res.render('gigs', { gigs }))
    .catch((err) => res.render('error', { error: err }));
});

module.exports = router;
